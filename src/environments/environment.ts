// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const url_config = {
  domain : "http://localhost:",
  port : "8070"
}

export const environment = {
  production: false,
  url: `${url_config.domain}${url_config.port}`,
  account : {
    base : `${url_config.domain}${url_config.port}/api/v1/socket/login/user`,
    new_user : `${url_config.domain}${url_config.port}/api/v1/socket/login/user/new`,
    get_user : `${url_config.domain}${url_config.port}/api/v1/socket/login/logged-in`,
  },
  messages : {
    base : `${url_config.domain}${url_config.port}/api/v1/socket/messages`,
    typing : `${url_config.domain}${url_config.port}/api/v1/socket/messages/typing`
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
