const url_config = {
  domain : "http://3.19.174.165:",
  port : "8070"
}

export const environment = {
  production: true,
  url: `${url_config.domain}${url_config.port}`,
  account : {
    base : `${url_config.domain}${url_config.port}/api/v1/socket/login/user`,
    new_user : `${url_config.domain}${url_config.port}/api/v1/socket/login/user/new`,
    get_user : `${url_config.domain}${url_config.port}/api/v1/socket/login/logged-in`,
  },
  messages : {
    base : `${url_config.domain}${url_config.port}/api/v1/socket/messages`,
    typing : `${url_config.domain}${url_config.port}/api/v1/socket/messages/typing`
  }
};
