import { Component, OnInit, ViewChild, ElementRef, AfterContentInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { environment } from 'src/environments/environment';
import { Message } from '../message';
import { SocketService } from '../socket.service';
import { concatMap, map, zip, tap, timeout } from 'rxjs/operators';
import { LoginService } from '../login.service';
import { UserDetails } from '../user-details';
import { AuthService } from 'angularx-social-login';
import { of, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-code-socket',
  templateUrl: './code-socket.component.html',
  styleUrls: ['./code-socket.component.css']
})
export class CodeSocketComponent implements OnInit{
  
  @ViewChild("messageBox", {static : true}) messageBox : ElementRef;
  @ViewChild("textAreaMessage", {static: true}) textArea : ElementRef;

  private serverUrl = environment.url + '/socket'
  isLoaded: boolean = false;
  isCustomSocketOpened = false;
  stompClient;
  form: FormGroup;
  userForm: FormGroup;
  loggedInUser : UserDetails;
  messages: Message[] = [];
  tmp : Message[] = [];

  private pageNumber; 
  private totalPages; 

  isFetching : boolean = true;

  constructor(
    private socketService : SocketService,
    private loginService : LoginService,
    private authService : AuthService,
    private messageService : MessageService,
    private router : Router){
      
  }

  ngOnInit() {

    this.form = new FormGroup({
      message: new FormControl(null, [Validators.required])
    })
    
    this.authService.authState
    .pipe(
      concatMap(response => {
        if(response != null){
          const facebookId = response.id;
          return this.loginService.getLoggedInUserDetailsByFacebookId(facebookId);
        }
          else return of(null);
      })
    )
    .subscribe(response => {
      if(response != null){
        this.loggedInUser = UserDetails.fromApiResponse(response);
        this.userForm = new FormGroup({
          fromId: new FormControl(this.loggedInUser.handler, [Validators.required]),
          toId: new FormControl("")
        })
        this.fetchMessages();
        console.log(this.loggedInUser);
      }else 
      this.router.navigate(["login"])
    })
    this.initializeWebSocketConnection();
  }

  

  fetchMessages(pageNumber? : string){

    this.isFetching = true;
   
    this.messageService.fetchAll(pageNumber)
    .subscribe(response => {
      setTimeout(() => {
        this.messages.reverse()
      response.content.map(m => {
        let message : Message =  {message : m.message, sender : m.sender , recipient : m.recipient}
        this.messages.push(message);
        this.pageNumber = response.pageable.pageNumber;
        this.totalPages = response.pageable.totalPages;  
      })
      this.messages.reverse();
      this.isFetching = false;


      }, 1000)
      
      
      
    })
   
  }

  beingScrolled(evt){

    if(this.messageBox.nativeElement.scrollTop == 0){
      this.retrievePreviousMessages();
    }
  }

  textAreaChange(evt){

    const user = { user : this.loggedInUser }

    this.stompClient.send("/socket-subscriber/send/typing", {}, JSON.stringify(user))

    if(evt.key == "Enter" && evt.key == "Shift"){

    }else if(evt.key === "Enter"){
      this.sendMessageUsingRest();
    }
  }

  sendMessageUsingSocket() {
    if (this.form.valid) {
      let message: Message = { message: this.form.value.message, sender: this.userForm.value.fromId, recipient: this.userForm.value.toId };
      this.stompClient.send("/socket-subscriber/send/message", {}, JSON.stringify(message));
    }
  }

  sendMessageUsingRest() {
    if (this.form.valid) {
      let message: Message = { message: this.form.value.message, sender: this.userForm.value.fromId, recipient: this.userForm.value.toId };
      this.socketService.post(message).subscribe(
        response => {
          console.log(response);
          this.form.reset();
          this.scrollToBottom();
        }
      )
    }
  }

  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.isLoaded = true;
      that.openGlobalSocket();
      that.openTypingListenerSocket();
    });
  }

  openGlobalSocket() {
    this.stompClient.subscribe("/socket-publisher", (message) => {
      this.handleResult(message);
    });
  }

  openTypingListenerSocket(){
    this.stompClient.subscribe("/socket-publisher/typing", (message) => {
      this.handleTyping(message);
    })
  }
  
  handleResult(message){

    if (message.body) {
      console.log(message);
      let messageResult: Message = JSON.parse(message.body);
      console.log(messageResult);
      this.messages.push(messageResult)
      setTimeout(() => {this.scrollToBottom()}, 100);      
      
    }

  }

  scrollToBottom(){
    this.messageBox.nativeElement.scrollTop = this.messageBox.nativeElement.scrollHeight;          
  }

  handleTyping(message){
    console.log(message);
  }

  retrievePreviousMessages(){
    this.fetchMessages(this.pageNumber + 1);
  }



}