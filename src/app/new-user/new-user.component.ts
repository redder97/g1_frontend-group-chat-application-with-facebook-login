import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../login.service';
import { UserDetails } from '../user-details';
import { AuthService, SocialUser } from 'angularx-social-login';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  form: FormGroup;
  socialUser : SocialUser;
  hasLoaded : boolean = false;

  constructor(
    private loginService : LoginService,
    private authService: AuthService,
    private router : Router

  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      handler : new FormControl(null, [Validators.required]),
    })

    this.authService.authState
    .subscribe(response => {
      this.socialUser = response;
      this.hasLoaded = true;
      console.log("This dude: _"  + this.socialUser.id);
    })
  }
  
  newUser(){
    if(this.form.valid){
      console.log("submitting with this id: " + this.socialUser);
      const newUser = UserDetails.createNewUser( this.socialUser.id, this.socialUser.email, this.form.value.handler);
      this.loginService.newUser(newUser).subscribe(
        response => {
          console.log("created User: " + response);
          this.router.navigate(["chat"])

        }
      )
    }
  }
}
