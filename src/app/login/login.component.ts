import { Component, OnInit } from '@angular/core';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { Router } from '@angular/router';
import { LoginService } from '../login.service';
import { UserDetails } from '../user-details';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  title = 'chatR';
  user: SocialUser;
  loggedIn: boolean;

  constructor(
    private authService: AuthService,
    private router: Router,
    private loginService : LoginService) { }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID)
    .then(response => {
      const user : UserDetails = UserDetails.fromSuccessLoginFacebook(response);
      return this.loginService.login(user);
    })
    .then(response => {
      response.subscribe(response => {
        this.loginResponseHandler(response);
      })
    })
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID)
    .then(response => {
      const user : UserDetails = UserDetails.fromSuccessLoginGoogle(response);
      console.log(response);
      return this.loginService.login(user);
    })
    .then(response => {
      response.subscribe(response => {
        this.loginResponseHandler(response);
      })
    })
  }



  signOut(): void {
    this.authService.signOut();
  }

  loginResponseHandler(response : any){
    if(response.existingUser){
      this.router.navigate(["chat"]);
    }else
      this.router.navigate(["new-user"]);
  }

  ngOnInit() {
    if (localStorage.fbToken) {
      this.loggedIn = true;
    }

    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      console.log(this.user);
      
    });
  }
}
