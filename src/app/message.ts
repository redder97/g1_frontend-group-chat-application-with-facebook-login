export interface Message {
    message: string,
    sender: string,
    recipient: string,
}