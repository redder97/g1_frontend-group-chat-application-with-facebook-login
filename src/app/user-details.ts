export class UserDetails {
    socialUserId : string;
    name : string;
    accessToken : string;
    email : string;
    handler : string;

    public constructor(){}

    public static fromSuccessLoginFacebook (successResponse : any)  : UserDetails {
        const user : UserDetails = new UserDetails();

        user.socialUserId = successResponse.id;
        user.name = successResponse.facebook.name;
        user.accessToken = successResponse.authToken;
        user.email = successResponse.facebook.email;

        return user;
    }

    public static fromSuccessLoginGoogle (successResponse : any)  : UserDetails {
        const user : UserDetails = new UserDetails();

        user.socialUserId = successResponse.id;
        user.name = successResponse.name;
        user.accessToken = successResponse.authToken;
        user.email = successResponse.email;

        return user;
    }

    public static fromApiResponse (successResponse : any) : UserDetails {
        const user : UserDetails = new UserDetails();

        user.socialUserId = successResponse.socialUserId;
        user.name = successResponse.name;
        user.email = successResponse.email;
        user.handler = successResponse.handler;

        return user;
    }

    public static createNewUser (socialUserId : string, email : string, handler : string) : UserDetails {
        const user : UserDetails = new UserDetails();

        user.socialUserId = socialUserId;
        user.email = email;
        user.handler = handler;

        return user;
    }
    
}
