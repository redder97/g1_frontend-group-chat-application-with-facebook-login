import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Message } from './message';
import { concatMap, map, zip, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SocketService {
  url: string = environment.url + "/api/v1/socket";

  constructor(private http: HttpClient) { }

  post(data: Message) {
    return this.http.post(this.url, data)
      .pipe(
        map((data: Message) => { return data; })
      )
  }

}