import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private httpClient : HttpClient) { }

  public fetchAll(pageNumber? : string) : Observable<any> {
    return this.httpClient.get(environment.messages.base + "?page=" + pageNumber)
  }

}
