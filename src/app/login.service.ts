import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserDetails } from './user-details';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };


  public login(user : UserDetails) : Observable<any> {
    const loginCred = JSON.stringify(user);
    return this.httpClient.post(environment.account.base, loginCred, this.httpOptions)
    
  }

  public newUser(user : UserDetails) : Observable<any>{
    const newUser = JSON.stringify(user);
    return this.httpClient.post(environment.account.new_user, newUser, this.httpOptions);
  }

  public getLoggedInUserDetailsByFacebookId( facebookId : string){
    return this.httpClient.get(environment.account.get_user + "/" + facebookId);
  } 

  constructor(private httpClient : HttpClient) {

  }
}
