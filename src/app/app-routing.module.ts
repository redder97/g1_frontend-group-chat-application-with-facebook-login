import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CodeSocketComponent } from './code-socket/code-socket.component';
import { LoginComponent } from './login/login.component';
import { NewUserComponent } from './new-user/new-user.component';


const routes: Routes = [
  {path : 'chat', component: CodeSocketComponent, pathMatch: 'full'},
  {path : 'login', component: LoginComponent, pathMatch: 'full'},
  {path : 'new-user', component: NewUserComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
